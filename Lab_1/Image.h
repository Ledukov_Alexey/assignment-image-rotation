#ifndef UNTITLED7_IMAGE_H
#define UNTITLED7_IMAGE_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct pixel { uint8_t r, g, b; };
struct image create_img(const uint64_t w, const uint64_t h);
void destroy_img(struct image *img);

enum read_status from_bmp( FILE* in, struct image* img);
uint8_t save_BMP(FILE *file, struct image* img2, const char *filename);
#endif //UNTITLED7_IMAGE_H
