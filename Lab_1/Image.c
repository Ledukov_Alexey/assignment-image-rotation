#include "Image.h"

struct image create_img(const uint64_t w, const uint64_t h){
    struct image img={0};
    img.width = w;
    img.height = h;
    img.data = (struct pixel*)malloc(w*h*sizeof(struct pixel));
    //img.data = (struct pixel*)calloc(w*h, sizeof(struct pixel));
    return img;
}
void destroy_img(struct image *img){
    if(img->data!=NULL){
        free(img->data);
        img->data=NULL;
    }
}





