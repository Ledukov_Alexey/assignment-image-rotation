#include "Image.h"
#include "Processing.h"

/* ᮧ  ࠦ,    90 ࠤᮢ */
struct image rotate(struct image const source) {
    const uint32_t my=source.width;
    const uint32_t mx=source.height;
    struct pixel p;
    struct image dest;

    dest=create_img(my, mx);
    for (uint32_t x = 0; x < mx ; x++) {
        for (uint32_t y = 0; y < my; y++) {
            uint32_t n1=mx*y+x;// 筨
            uint32_t n2=my*(x+1)-y-1;// 
            p=source.data[n1];
            dest.data[n2]=p;
        }
    }
    return dest;
}
