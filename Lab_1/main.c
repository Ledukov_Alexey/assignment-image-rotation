#include "Status.h"
#include "Image.h"
#include "Processing.h"
#include "Speaks.h"
#define buf_size 1000

int main(){
    char str[1001];
    struct image img1={0}, img2={0};
    const char* filename;
    const char* name = "Enter the file name to read, for example: C:\\Ci\\11111.bmp ";
    filename = read_filename(str, name,buf_size);
    FILE* file=fopen(filename, "rb");
    if(file == NULL) {
        printf("File <%s> not found", filename);
        return 6;
    }
    enum read_status rs=from_bmp( file, &img1);
    fclose(file);
    if (rs==READ_OK) {
        name = "Enter a file name for the record, for example: C:\\Ci\\2.bmp";
        filename = read_filename(str, name,buf_size);
        file=fopen(filename, "wb");
        if (file == NULL) {
            fprintf(stderr,"Error writing to file <%s>", filename);
            fclose(file);
            destroy_img(&img1);
            return WRITE_ERROR;
        }
        img2=create_img(img1.height, img1.width);
        img2 = rotate(img1);
        destroy_img(&img1);
        return save_BMP(file, &img2, filename);
    }
    fprintf(stderr,"Error reading to file<%s>", filename);
    return 1;
}//main




