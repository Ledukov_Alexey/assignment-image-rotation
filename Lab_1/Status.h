#define BM 0x4D42
#ifndef UNTITLED7_LIBR_H
#define UNTITLED7_LIBR_H
enum read_status  {
    READ_OK = 0,
    //READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3,
    FILE_NOT_FOUND = 4
    /* коды других ошибок  */
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1
    /* коды других ошибок  */
};
#endif //UNTITLED7_LIBR_H
