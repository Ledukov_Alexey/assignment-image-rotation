#include "Status.h"
#include "Image.h"
#define BMP_HEADER( Bmp ) \
        Bmp( uint16_t,bfType)\
        Bmp( uint32_t,bfileSize)\
        Bmp( uint32_t,bfReserved)\
        Bmp( uint32_t,bOffBits)\
        Bmp( uint32_t,biSize)\
        Bmp( uint32_t,biWidth)\
        Bmp( uint32_t,biHeight)\
        Bmp( uint16_t,biPlanes)\
        Bmp( uint16_t,biBitCount)\
        Bmp( uint32_t,biCompression)\
        Bmp( uint32_t,biSizeImage)\
        Bmp( uint32_t,biXPelsPerMeter)\
        Bmp( uint32_t,biYPelsPerMeter)\
        Bmp( uint32_t,biClrUsed)\
        Bmp( uint32_t,biClrImportant)
#define DECLARING( a, b ) a b ;

static uint32_t padding(const uint64_t mx){return (3*mx+3) & (-4);}
struct __attribute__((packed)) bmp_header{BMP_HEADER( DECLARING )};
struct bmp_header create_bmp_header( const uint32_t w, const uint32_t h){
    struct bmp_header bmpH={0};
    bmpH.bfType=BM;
    bmpH.bfileSize=54+padding(w)*h;
    bmpH.bfReserved=0;
    bmpH.bOffBits=sizeof(struct bmp_header);
    bmpH.biSize=40;
    bmpH.biWidth=h;
    bmpH.biHeight=w;
    bmpH.biPlanes=1;
    bmpH.biBitCount=24;
    bmpH.biCompression=0;
    bmpH.biSizeImage=bmpH.bfileSize-bmpH.bOffBits;
    bmpH.biXPelsPerMeter=4724;
    bmpH.biYPelsPerMeter=4724;
    bmpH.biClrUsed=0;
    bmpH.biClrImportant=0;
    return bmpH;
}
enum read_status read_header( FILE* in,struct bmp_header* bmpH1) {
    const size_t str=fread(bmpH1, sizeof(struct bmp_header), 1, in);
    if ((str!=1) || (bmpH1->bfType != BM) || (bmpH1->biBitCount != 24)) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}//read_header
enum write_status write_header( FILE* file, struct bmp_header const bmpH2){
    if (fwrite(&bmpH2, sizeof(bmpH2), 1, file)==1){
        return WRITE_OK;
    }
    return WRITE_ERROR;
}//write_header
enum read_status from_bmp( FILE* in, struct image* img) {
    struct bmp_header bmpH1={0};
    enum read_status rs=read_header(in,&bmpH1);
    if (rs==READ_OK){
        const uint32_t mx = bmpH1.biWidth;
        const uint32_t my = bmpH1.biHeight;
        *img=create_img(my, mx);
        //считывание файла
        for(uint32_t i = 0; i < my; ++i){
            const uint32_t n=mx*i;
            if (fread(&(img->data[n]), sizeof(struct pixel), mx, in)!=mx){
                free(img->data);
                return READ_INVALID_BITS;
            }
            const uint32_t ost=padding(mx)-mx*3;
            uint64_t tmp=0;
            if (fread(&tmp, sizeof(uint8_t), ost, in)!=ost){
                return READ_INVALID_BITS;
            }
        }
        return READ_OK;
    }
    return READ_INVALID_HEADER;
}//from_bmp
enum write_status to_bmp( FILE* file, struct image* img){
    struct bmp_header const bmpH2=create_bmp_header(img->height, img->width);
    enum write_status ws = write_header(file, bmpH2);
    if (ws==WRITE_OK){
        const uint32_t mx = bmpH2.biWidth;
        const uint32_t my = bmpH2.biHeight;
        for(uint32_t i = 0; i < my; ++i){
            uint32_t n=mx*i;
            if (fwrite(&img->data[n], sizeof(struct pixel), mx, file)!=mx){
                return WRITE_ERROR;
            }
            const uint32_t ost=padding(mx)-mx*3;
            uint64_t tmp=0;
            if (fread(&tmp, sizeof(uint8_t), ost, file)!=ost){
                return WRITE_ERROR;
            }
        }
        return WRITE_OK;
    }
    destroy_img(img);
    return WRITE_ERROR;
}//to_bmp
uint8_t save_BMP(FILE *file, struct image* img2, const char *filename) {
    enum write_status ws = to_bmp(file, img2);
    destroy_img(img2);
    fclose(file);
    if (ws==WRITE_OK){
        printf("The file <%s> was created successfully", filename);
        return 0;
    }
    fprintf(stderr,"Error writing to file <%s>", filename);
    return 5;
}//save_BMP